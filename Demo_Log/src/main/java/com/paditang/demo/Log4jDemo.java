package com.paditang.demo;

import org.apache.log4j.Logger;

public class Log4jDemo {

	private static final Logger log = Logger.getLogger(Log4jDemo.class);
	
	public static void main(String[] args){
		log.debug("debug msg");
		log.info("info msg");
		log.warn("warn msg");
		log.error("error msg");
		log.fatal("fatal msg");
	}
}
